import { Component, ViewChild, ViewChildren } from '@angular/core';
import { StorageService, Item } from '../services/storage.service';
import { Platform, ToastController, IonList } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  items: Item[] = [];

  newItem: Item = <Item>{};

  @ViewChildren('mylist') mylist: IonList;
 

  constructor(private storageService: StorageService, private plt: Platform, private toastController: ToastController) {
    this.plt.ready().then(() => {
      this.loadItems();
    });
  }

  addItem(){
    this.newItem.data = this.newItem.data;
    this.newItem.id = this.newItem.id;
    this.newItem.detalhesdoitem = this.newItem.detalhesdoitem;
    this.newItem.nome = this.newItem.nome;
    this.newItem.emprestado = this.newItem.emprestado;

    this.storageService.addItem(this.newItem).then(item => {
      this.newItem = <Item>{};
      this.showToast('Item Adicionado');
      this.loadItems();
    });
  }

  // LER
  loadItems(){
    this.storageService.getItems().then(items => {
      this.items = items;
    });    
  }

  //Atualizar
  updateItem(item: Item){
    item.nome = 'UPDATED: ${item.nome}';
    

    this.storageService.updateItem(item).then(item => {
      this.showToast('Item Atualizado!');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
  }

  //deletar
  deleteItem(item: Item){
    this.storageService.deleteItem(item.id).then(item => {
      this.showToast('Item Removido');
      this.mylist.closeSlidingItems();
      this.loadItems();
    });
  }

  async showToast(msg){
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

}
