import { Injectable } from '@angular/core';

export interface Item{
  id: number;
  nome: string;
  emprestado: string;
  data: Date;
  detalhesdoitem: string;
}

const ITEMS_KEY = 'meus-items';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) { }
  
  //criar
  addItem(item: Item): Promise<any>{
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (items){
        items.push(item);
        return this.storage.set(ITEMS_KEY, [item]);
      } else{
        return this.storage.set(ITEMS_KEY, [item]);
      }
    });
  }

   //ler
   getItems(): Promise<Item[]> {
    return this.storage.get(ITEMS_KEY);
  }

   //atualizar
   updateItem(item: Item): Promise<any>{
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0){
      return null;
      }

      let newItems: Item[] = [];
      
      for (let i of items){
        if(i.id === item.id){
          newItems.push(item);
        }else{
          newItems.push(i);
        }
      }

      return this.storage.set(ITEMS_KEY, newItems);
    });
  }

   //apagar
   deleteItem(id: number): Promise<Item>{
    return this.storage.get(ITEMS_KEY).then((items: Item[]) => {
      if (!items || items.length === 0){
        return null;
        }

        let toKeep: Item[] = [];

        for(let i of items){
          if(i.id !== id){
            toKeep.push(i);
          }
        }

        return this.storage.set(ITEMS_KEY, toKeep);
    });
  }
}
